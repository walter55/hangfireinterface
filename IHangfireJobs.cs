﻿using Hangfire.Server;
using ModelProcessor.Worker.ConvertIfc4x1.Ifc4x1Entities;
using System.Collections.Generic;

namespace HangfireInterface
{
    public interface ISquirrelJobs
    {
        void CreateRevisionAsync
        (
            string dbName,
            string ifcFileName,
            string wexBimFileName,
            int revId,
            int pmId,
            Dictionary<int, IFC4x1Base> entitiesIfc4x1,
            PerformContext context
        );

    }
}
